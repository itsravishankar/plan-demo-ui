package com.example.ravi.plandemo.fragment.onboarding;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.FragmentOnboardingOneBinding;
import com.example.ravi.plandemo.databinding.FragmentOnboardingTwoBinding;

/**
 * Created by ravi on 24/4/18.
 */

public class OnBoardingFragmentTwo extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentOnboardingTwoBinding fragmentOnboardingTwoBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_onboarding_two, container, false);
        return fragmentOnboardingTwoBinding.getRoot();
    }
}
