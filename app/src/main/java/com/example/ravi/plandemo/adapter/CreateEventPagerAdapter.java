package com.example.ravi.plandemo.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.ravi.plandemo.fragment.createEvent.CreateEventAddNameDetailsFragment;
import com.example.ravi.plandemo.fragment.createEvent.CreateEventDateTimePickerFragment;

/**
 * Created by ravi on 26/4/18.
 */

public class CreateEventPagerAdapter extends FragmentPagerAdapter {
    public CreateEventPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {

            case 0:
                return new CreateEventAddNameDetailsFragment();
            case 1:
                return new CreateEventDateTimePickerFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
