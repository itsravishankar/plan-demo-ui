package com.example.ravi.plandemo.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.ActivitySplashScreenBinding;

public class SplashActivity extends BaseActivity {

    int SPLASH_TIME = 1500;

    @Override
    protected void initUi() {
        ActivitySplashScreenBinding activitySplashScreenBinding = (ActivitySplashScreenBinding) viewDataBinding;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(
                        SplashActivity.this, OnBoardingActivity.class));
                finish();
            }
        }, SPLASH_TIME);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_splash_screen;
    }

    @Override
    public void onClick(View v) {

    }
}
