package com.example.ravi.plandemo.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.adapter.OnBoardingAdapter;
import com.example.ravi.plandemo.databinding.ActivityOnboardingBinding;
import com.example.ravi.plandemo.fragment.onboarding.OnBoardingFragmentOne;
import com.example.ravi.plandemo.fragment.onboarding.OnBoardingFragmentThree;
import com.example.ravi.plandemo.fragment.onboarding.OnBoardingFragmentTwo;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by ravi on 24/4/18.
 */

public class OnBoardingActivity extends BaseActivity {

    private ViewPager mViewPager;

    @Override
    protected void initUi() {
        ActivityOnboardingBinding activityOnboardingBinding = (ActivityOnboardingBinding) viewDataBinding;
        OnBoardingAdapter onBoardingAdapter = new OnBoardingAdapter(getSupportFragmentManager());
        CircleIndicator mCircleIndicator = activityOnboardingBinding.indicatorViewPager;
        mViewPager = activityOnboardingBinding.viewPagerOnBoarding;
        mViewPager.setAdapter(onBoardingAdapter);
        mCircleIndicator.setViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(4);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_onboarding;
    }

    @Override
    public void onBackPressed() {
        Fragment fragmentManager = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.view_pager_on_boarding + ":" + mViewPager.getCurrentItem());
        if (fragmentManager instanceof OnBoardingFragmentOne){
            super.onBackPressed();
        } else if (fragmentManager instanceof OnBoardingFragmentTwo){
            mViewPager.setCurrentItem(0);
        } else if (fragmentManager instanceof OnBoardingFragmentThree){
            mViewPager.setCurrentItem(1);
        }

    }

    @Override
    public void onClick(View v) {

    }
}
