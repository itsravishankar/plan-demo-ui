package com.example.ravi.plandemo.fragment.setupprofile;

import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.FragmentCreatePasswordBinding;
import com.example.ravi.plandemo.fragment.BaseFragment;

/**
 * Created by ravi on 24/4/18.
 */

public class SignupCreatePasswordFragment extends BaseFragment {

    private boolean isShow;
    private EditText mEditTextPassword;
    private ImageView mImageViewPasswordValidationMsg;
    private TextView mTextViewShowPasswordButton;
    private FragmentCreatePasswordBinding fragmentCreatePasswordBinding;


    @Override
    protected void initUi() {
        fragmentCreatePasswordBinding = (FragmentCreatePasswordBinding) viewDataBinding;
        mEditTextPassword = fragmentCreatePasswordBinding.editTextPassword;
        mImageViewPasswordValidationMsg = fragmentCreatePasswordBinding.imageViewPasswordValidationIcon;
        mTextViewShowPasswordButton = fragmentCreatePasswordBinding.tvShowPasswordButton;
        mTextViewShowPasswordButton.setOnClickListener(this);

        mEditTextPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mImageViewPasswordValidationMsg.getVisibility() == View.GONE)
                    mImageViewPasswordValidationMsg.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() >= 8) {
                    setImageViewAnimatedChange(context, mImageViewPasswordValidationMsg, R.drawable.ic_done_white_16dp_2x);
                } else
                    setImageViewAnimatedChange(context, mImageViewPasswordValidationMsg, R.drawable.ic_clear_white_18dp);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_create_password;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tv_show_password_button:
                showPassword();
                break;
        }
    }

    private void showPassword() {
        if (!isShow) {
            isShow = true;
            mEditTextPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            mEditTextPassword.setSelection(mEditTextPassword.getText().toString().length());
        } else {
            isShow = false;
            mEditTextPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }
}
