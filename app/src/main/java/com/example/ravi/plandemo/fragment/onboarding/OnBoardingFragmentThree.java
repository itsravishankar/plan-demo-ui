package com.example.ravi.plandemo.fragment.onboarding;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.activity.SignupLoginActivty;
import com.example.ravi.plandemo.databinding.FragmentOnboardingThreeBinding;
import com.example.ravi.plandemo.databinding.FragmentOnboardingTwoBinding;

/**
 * Created by ravi on 24/4/18.
 */

public class OnBoardingFragmentThree extends Fragment {

    private Button mButtonGetStarted;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentOnboardingThreeBinding fragmentOnboardingThreeBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_onboarding_three, container, false);

        mButtonGetStarted = fragmentOnboardingThreeBinding.buttonGetStarted;
        return fragmentOnboardingThreeBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
         mButtonGetStarted.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 startActivity(new Intent(getActivity(), SignupLoginActivty.class));
                 getActivity().finish();
             }
         });
    }
}
