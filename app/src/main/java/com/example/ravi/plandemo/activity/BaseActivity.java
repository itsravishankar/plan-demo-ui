package com.example.ravi.plandemo.activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ravi.plandemo.R;


public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    protected ViewDataBinding viewDataBinding;
    public Toolbar mToolBar;
    public TextView titleTv;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewDataBinding = DataBindingUtil.setContentView(this, getLayoutById());

        mToolBar = (Toolbar) findViewById(R.id.toolbar_main_screen);
        titleTv = (TextView) findViewById(R.id.textView_toolbar_main_screen);
        initUi();
    }
    /**
     * Method to set toolbar title
     *
     * @param title Title to set
     */
    protected void setHeaderTitle(String title) {
        if (titleTv != null) {
            titleTv.setText(title);
        }
    }

    /**
     * Method to return this object
     *
     * @return Object of this class
     */
    protected BaseActivity getActivity() {
        return this;
    }

    /**
     * Initilize Ui parameters here
     */
    protected abstract void initUi();

    /**
     * Returns the layout resource identifier
     * @return Layout res id
     */
    protected abstract int getLayoutById();

    public static final String TAG = "DEBUG";
    public void setImageViewAnimatedChange(Context c, final ImageView v, final int id) {
        final Bitmap newImage = BitmapFactory.decodeResource(getResources(), id);
        final Animation animOut = AnimationUtils.loadAnimation(c, R.anim.zoom_out);
        final Animation animIn  = AnimationUtils.loadAnimation(c, R.anim.zoom_in);
        animOut.setAnimationListener(new Animation.AnimationListener()
        {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation)
            {
                v.setImageBitmap(newImage);
                animIn.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {}
                });
                v.startAnimation(animIn);
            }
        });
        v.startAnimation(animOut);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


}
