package com.example.ravi.plandemo.fragment.setupprofile;

import android.view.View;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.FragmentMobileConfirmationCodeBinding;
import com.example.ravi.plandemo.fragment.BaseFragment;


public class SignupMobileNoConfirmationFragment extends BaseFragment {

    private FragmentMobileConfirmationCodeBinding fragmentMobileConfirmationCodeBinding;

    @Override
    protected void initUi() {
    fragmentMobileConfirmationCodeBinding = (FragmentMobileConfirmationCodeBinding) viewDataBinding;
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_mobile_confirmation_code;
    }

    @Override
    public void onClick(View view) {

    }
}
