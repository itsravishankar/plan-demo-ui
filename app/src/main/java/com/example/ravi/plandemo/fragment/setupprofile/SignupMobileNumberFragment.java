package com.example.ravi.plandemo.fragment.setupprofile;

import android.os.Build;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.FragmentSignupMobileNumberBinding;
import com.example.ravi.plandemo.fragment.BaseFragment;


public class SignupMobileNumberFragment extends BaseFragment {

    private FragmentSignupMobileNumberBinding fragmentSignupMobileNumberBinding;
    private Button mButtonContinue;
    private EditText mEditTextPhoneNo;

    @Override
    protected void initUi() {
        fragmentSignupMobileNumberBinding = (FragmentSignupMobileNumberBinding) viewDataBinding;
        ;
        mEditTextPhoneNo = fragmentSignupMobileNumberBinding.editTextPhoneNumber;
       // mEditTextPhoneNo.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        mEditTextPhoneNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    System.out.println(PhoneNumberUtils.formatNumber(mEditTextPhoneNo.getText().toString(),"CA" ));
                }
            }
        });


    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_signup_mobile_number;
    }

    @Override
    public void onClick(View view) {

    }
}
