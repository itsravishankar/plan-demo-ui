package com.example.ravi.plandemo.fragment.setupprofile;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.FragmentSignupAddProfilepicBinding;
import com.example.ravi.plandemo.fragment.BaseFragment;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

import id.zelory.compressor.Compressor;
import pl.aprilapps.easyphotopicker.EasyImage;

import static com.example.ravi.plandemo.activity.BaseActivity.TAG;

/**
 * Created by ravi on 24/4/18.
 */

public class SignupAddProfilePicFragment extends BaseFragment {

    private int CAMERA_REQUEST = 0;
    private int REQUEST_CAMERA = 1;
    private int REQUEST_GALLERY = 2;
    private int STORAGE_REQUEST = 3;
    private String mFilePath;
    private ImageView mImageViewProfilePic;
    private FragmentSignupAddProfilepicBinding fragmentSignupAddProfilepicBinding;

    @Override
    protected void initUi() {
        fragmentSignupAddProfilepicBinding = (FragmentSignupAddProfilepicBinding) viewDataBinding;
        mImageViewProfilePic = fragmentSignupAddProfilepicBinding.imageViewAddPic;

        mImageViewProfilePic.setOnClickListener(this);
    }

    private void openImagePickerDialog() {
        final CharSequence[] items = {"Camera", "Gallery", "Cancle"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Image");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals(items[0])) {
                    onPickImageFromCamera();
                } else if (items[i].equals(items[1])) {
                    onPickImageFromGalery();
                } else if (items[i].equals(items[2])) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();
    }

    private void onPickImageFromGalery() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_REQUEST);
        } else {
            EasyImage.openGallery(this, REQUEST_GALLERY);
        }
    }

    private void onPickImageFromCamera() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST);
        } else {
            EasyImage.openCamera(this, REQUEST_CAMERA);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == CAMERA_REQUEST) {
                EasyImage.openCamera(this, REQUEST_CAMERA);
            } else if (requestCode == STORAGE_REQUEST) {
                EasyImage.openGallery(this, REQUEST_GALLERY);
            }
        } else
            Toast.makeText(context, getResources().getString(R.string.pic_permission_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        EasyImage.handleActivityResult(requestCode, resultCode, data, (Activity) context, new EasyImage.Callbacks() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                e.printStackTrace();
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                onImageRecieved(imageFile, source);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                if (source == EasyImage.ImageSource.CAMERA) {
                    File picFile = EasyImage.lastlyTakenButCanceledPhoto(context);
                    picFile.delete();
                }
            }
        });

    }

    private void onImageRecieved(File imageFile, EasyImage.ImageSource source) {
        File compressedImageFile = null;
        try {
            compressedImageFile = new Compressor(context).compressToFile(imageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (compressedImageFile != null) {
            mFilePath = compressedImageFile.getAbsolutePath();
            Log.d(TAG, "onImageRecieved:File Path " + mFilePath);
        }
        Picasso.get().load(new File(mFilePath)).into(mImageViewProfilePic);
    }



    @Override
    protected int getLayoutById() {
        return R.layout.fragment_signup_add_profilepic;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageView_add_pic:
                openImagePickerDialog();
                break;
        }
    }
}
