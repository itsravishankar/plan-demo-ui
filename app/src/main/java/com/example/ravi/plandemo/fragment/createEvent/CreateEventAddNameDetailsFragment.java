package com.example.ravi.plandemo.fragment.createEvent;

import android.view.View;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.FragmentCreateEventNameDetailsBinding;
import com.example.ravi.plandemo.fragment.BaseFragment;

/**
 * Created by ravi on 26/4/18.
 */

public class CreateEventAddNameDetailsFragment extends BaseFragment {
    @Override
    protected void initUi() {
        FragmentCreateEventNameDetailsBinding fragmentCreateEventNameDetailsBinding
                = (FragmentCreateEventNameDetailsBinding) viewDataBinding;

    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_create_event_name_details;
    }

    @Override
    public void onClick(View v) {

    }
}
