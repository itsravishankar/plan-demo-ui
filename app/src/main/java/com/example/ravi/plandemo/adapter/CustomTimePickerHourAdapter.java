package com.example.ravi.plandemo.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.ItemTimePickerBinding;


public class CustomTimePickerHourAdapter extends RecyclerView.Adapter<CustomTimePickerHourAdapter.CustomTimePickerHourViewHolder>{

    private Context context;

    public CustomTimePickerHourAdapter(Context context) {
        this.context = context;
    }

    @Override
    public CustomTimePickerHourViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ItemTimePickerBinding itemTimePickerBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_time_picker, parent, false);

        return new CustomTimePickerHourViewHolder(itemTimePickerBinding);
    }

    @Override
    public void onBindViewHolder(CustomTimePickerHourViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class CustomTimePickerHourViewHolder extends RecyclerView.ViewHolder {
        public CustomTimePickerHourViewHolder(ItemTimePickerBinding itemView) {
            super(itemView.getRoot());
        }
    }
}
