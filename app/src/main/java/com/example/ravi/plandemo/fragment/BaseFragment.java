package com.example.ravi.plandemo.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.ravi.plandemo.R;

/**
 * Created by ravi on 25/4/18.
 */

public abstract class BaseFragment extends Fragment implements View.OnClickListener{

    private View view;
    protected Context context;
    protected ViewDataBinding viewDataBinding;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutById(), container, false);
       if (viewDataBinding != null)
           return viewDataBinding.getRoot();
       else return inflater.inflate(getLayoutById(), container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        initUi();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    protected View findViewById(int resId){ return view.findViewById(resId);}

    /**
     * Initilize Ui parameters here
     */
    protected abstract void initUi();

    /**
     * Returns the layout resource identifier
     * @return Layout res id
     */
    protected abstract int getLayoutById();

    /**
     * Animates the validation images used when user enters wrong argument
     * @param c Context
     * @param v ImageView
     * @param id int Resource id
     */
    public void setImageViewAnimatedChange(Context c, final ImageView v, final int id) {
        final Bitmap newImage = BitmapFactory.decodeResource(getResources(), id);
        final Animation animOut = AnimationUtils.loadAnimation(c, R.anim.zoom_out);
        final Animation animIn  = AnimationUtils.loadAnimation(c, R.anim.zoom_in);
        animOut.setAnimationListener(new Animation.AnimationListener()
        {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation)
            {
                v.setImageBitmap(newImage);
                animIn.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {}
                });
                v.startAnimation(animIn);
            }
        });
        v.startAnimation(animOut);
    }
}
