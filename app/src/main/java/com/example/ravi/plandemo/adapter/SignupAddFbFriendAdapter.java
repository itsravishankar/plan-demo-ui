package com.example.ravi.plandemo.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.ItemAddFriendRecyclerBinding;

/**
 * Created by ravi on 25/4/18.
 */

public class SignupAddFbFriendAdapter extends RecyclerView.Adapter<SignupAddFbFriendAdapter.SignupAddFbFriendViewHolder> {

    public SignupAddFbFriendAdapter() {

    }

    @Override
    public SignupAddFbFriendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemAddFriendRecyclerBinding itemAddFriendRecyclerBinding = DataBindingUtil
                .inflate(inflater, R.layout.item_add_friend_recycler, parent, false);
        return new SignupAddFbFriendViewHolder(itemAddFriendRecyclerBinding);
    }

    @Override
    public void onBindViewHolder(SignupAddFbFriendViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 20;
    }

    public class SignupAddFbFriendViewHolder extends RecyclerView.ViewHolder {
        public SignupAddFbFriendViewHolder(ItemAddFriendRecyclerBinding itemView) {
            super(itemView.getRoot());
        }
    }
}
