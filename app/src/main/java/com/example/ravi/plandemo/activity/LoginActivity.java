package com.example.ravi.plandemo.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.ActivityLoginBinding;


public class LoginActivity extends BaseActivity {

    private boolean isShow;
    private EditText mEditTextEmail, mEditTextPasswordLogin;
    private TextView mTextViewShowPassword, mTextViewForgetPassword;
    private Button mButtonContinueLogin;
    private ImageView mImageViewLoginEmailValidationMsg;


    @Override
    protected void initUi() {
        ActivityLoginBinding activityLoginBinding = (ActivityLoginBinding) viewDataBinding;
        mTextViewForgetPassword = activityLoginBinding.textViewForgetPassword;
        mEditTextEmail = activityLoginBinding.editTextEmailLogin;
        mEditTextPasswordLogin = activityLoginBinding.editTextPasswordLogin;
        mTextViewShowPassword = activityLoginBinding.textViewShowPassword;
        mButtonContinueLogin = activityLoginBinding.buttonContinueLogin;
        mImageViewLoginEmailValidationMsg = activityLoginBinding.imageViewLoginEmailValidationIcon;
        mTextViewForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, PasswordResetActivity.class));
            }
        });
        mTextViewShowPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPassword();
            }
        });

        mEditTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mImageViewLoginEmailValidationMsg.getVisibility() == View.GONE)
                    mImageViewLoginEmailValidationMsg.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Todo handle the validation of email
                if (charSequence.toString().contains("@") && charSequence.toString().contains(".com") || charSequence.toString().contains(".in") || charSequence.toString().contains(".net")) {
                    setImageViewAnimatedChange(LoginActivity.this, mImageViewLoginEmailValidationMsg, R.drawable.ic_done_white_16dp_2x);
                } else
                    setImageViewAnimatedChange(LoginActivity.this, mImageViewLoginEmailValidationMsg, R.drawable.ic_clear_white_18dp);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_login;
    }

    private void showPassword() {
        if (!isShow) {
            isShow = true;
            mEditTextPasswordLogin.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            mEditTextPasswordLogin.setSelection(mEditTextPasswordLogin.getText().toString().length());
        } else {
            isShow = false;
            mEditTextPasswordLogin.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }

    @Override
    public void onClick(View v) {

    }
}
