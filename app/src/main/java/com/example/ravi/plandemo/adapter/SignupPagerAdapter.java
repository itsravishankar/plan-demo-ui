package com.example.ravi.plandemo.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.ravi.plandemo.fragment.setupprofile.SignupAddFacebookFriendsFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupFindFacebookFriendsFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupAddProfilePicFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupBirthdayFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupCreatePasswordFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupEmailFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupMobileNoConfirmationFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupNameFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupMobileNumberFragment;


public class SignupPagerAdapter extends FragmentPagerAdapter {
    public SignupPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new SignupMobileNumberFragment();
            case 1:
                return new SignupMobileNoConfirmationFragment();
            case 2:
                return new SignupNameFragment();
            case 3:
                return new SignupEmailFragment();
            case 4:
                return new SignupBirthdayFragment();
            case 5:
                return new SignupCreatePasswordFragment();
            case 6:
                return new SignupAddProfilePicFragment();
            case 7:
                return new SignupFindFacebookFriendsFragment();
            case 8:
                return new SignupAddFacebookFriendsFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 9;
    }
}
