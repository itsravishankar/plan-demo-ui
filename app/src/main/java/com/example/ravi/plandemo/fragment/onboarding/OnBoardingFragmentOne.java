package com.example.ravi.plandemo.fragment.onboarding;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.FragmentOnboardingOneBinding;

/**
 * Created by ravi on 24/4/18.
 */

public class OnBoardingFragmentOne extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentOnboardingOneBinding fragmentOnboardingOneBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_onboarding_one, container, false);
        return fragmentOnboardingOneBinding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
