package com.example.ravi.plandemo.fragment.setupprofile;

import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.FragmentSignupBirthdayBinding;
import com.example.ravi.plandemo.fragment.BaseFragment;

import java.util.Calendar;
import java.util.Date;


public class SignupBirthdayFragment extends BaseFragment implements DatePicker.OnDateChangedListener {

    private EditText mEditTextBirthday;
    private FragmentSignupBirthdayBinding fragmentSignupBirthdayBinding;

    @Override
    public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {
        Log.d("month", String.valueOf(datePicker.getMonth()));
        Log.d("month", String.valueOf(i));
        Log.d("year", String.valueOf(datePicker.getYear()));
        Log.d("year", String.valueOf(datePicker.getDayOfMonth()));
        String month = getMonthName(i1);
        mEditTextBirthday.setText(month + "/" + i2 + "/" + i);
    }

    private String getMonthName(int i1) {
        String month = "";
        switch (i1) {
            case 0:
                month = "Jan";
                break;
            case 1:
                month = "Feb";
                break;
            case 2:
                month = "Mar";
                break;
            case 3:
                month = "Apr";
                break;
            case 4:
                month = "May";
                break;
            case 5:
                month = "June";
                break;
            case 6:
                month = "July";
                break;
            case 7:
                month = "Aug";
                break;
            case 8:
                month = "Sep";
                break;
            case 9:
                month = "Oct";
                break;
            case 10:
                month = "Nov";
                break;
            case 11:
                month = "Dec";
                break;
        }
        return month;
    }

    @Override
    protected void initUi() {
        fragmentSignupBirthdayBinding = (FragmentSignupBirthdayBinding) viewDataBinding;
        DatePicker mDatePicker = fragmentSignupBirthdayBinding.datePicker;
        mEditTextBirthday = fragmentSignupBirthdayBinding.editTextBirthday;
        mEditTextBirthday.setClickable(false);
        mEditTextBirthday.setText(getMonthName(mDatePicker.getMonth()) + "/" + mDatePicker.getDayOfMonth() + "/" + mDatePicker.getYear());
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mDatePicker.setOnDateChangedListener(this);
        } else {
            mDatePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), this);
        }*/
        mDatePicker.setMaxDate(calendar.getTimeInMillis());
        mDatePicker.init(calendar.get(Calendar.YEAR) - 17, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), this);


    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_signup_birthday;
    }

    @Override
    public void onClick(View view) {

    }
}
