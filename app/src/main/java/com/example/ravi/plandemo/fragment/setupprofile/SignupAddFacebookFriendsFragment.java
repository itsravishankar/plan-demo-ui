package com.example.ravi.plandemo.fragment.setupprofile;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.adapter.SignupAddFbFriendAdapter;
import com.example.ravi.plandemo.databinding.FragmentSignupAddFriendsDoneBinding;
import com.example.ravi.plandemo.fragment.BaseFragment;

/**
 * Created by ravi on 25/4/18.
 */

public class SignupAddFacebookFriendsFragment extends BaseFragment {

    private FragmentSignupAddFriendsDoneBinding fragmentSignupAddFriendsDoneBinding;

    @Override
    protected void initUi() {
        fragmentSignupAddFriendsDoneBinding = (FragmentSignupAddFriendsDoneBinding) viewDataBinding;
        RecyclerView mRecyclerView = fragmentSignupAddFriendsDoneBinding.recyclerViewAddFbFriends;
        SignupAddFbFriendAdapter addFbFriendAdapter = new SignupAddFbFriendAdapter();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(addFbFriendAdapter);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_signup_add_friends_done;
    }

    @Override
    public void onClick(View view) {

    }
}
