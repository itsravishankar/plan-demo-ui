package com.example.ravi.plandemo.fragment;

import android.view.View;

import com.example.ravi.plandemo.R;

/**
 * Created by ravi on 26/4/18.
 */

public class EventEmptyStateFragment extends BaseFragment {
    @Override
    protected void initUi() {

    }

    @Override
    protected int getLayoutById() {
        return R.layout.layout_event_feed_empty_state;
    }

    @Override
    public void onClick(View v) {

    }
}
