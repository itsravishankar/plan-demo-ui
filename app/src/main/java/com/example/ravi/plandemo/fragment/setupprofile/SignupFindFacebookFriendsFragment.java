package com.example.ravi.plandemo.fragment.setupprofile;

import android.view.View;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.fragment.BaseFragment;

/**
 * Created by ravi on 24/4/18.
 */

public class SignupFindFacebookFriendsFragment extends BaseFragment {


    @Override
    protected void initUi() {

    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_signup_find_fb_friends;
    }

    @Override
    public void onClick(View view) {

    }
}
