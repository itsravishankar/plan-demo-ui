package com.example.ravi.plandemo.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.adapter.SignupPagerAdapter;
import com.example.ravi.plandemo.customui.NonSwipeableViewPager;
import com.example.ravi.plandemo.databinding.ActivitySetupProfileBinding;
import com.example.ravi.plandemo.fragment.setupprofile.SignupAddFacebookFriendsFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupAddProfilePicFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupBirthdayFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupCreatePasswordFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupEmailFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupFindFacebookFriendsFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupMobileNoConfirmationFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupMobileNumberFragment;
import com.example.ravi.plandemo.fragment.setupprofile.SignupNameFragment;


public class SetupProfileActivity extends BaseActivity {


    private static final long TIME_INTERVAL = 2000;
    private Button mButtonSkip, mButtonDone;
    private TextView mButtonContinue;
    private ImageView mImageViewBackButton;
    private NonSwipeableViewPager nsvpContainer;
    private ActivitySetupProfileBinding activitySetupProfileBinding;
    private long mBackPressed;
    private RelativeLayout mRelativeLayoutSignupFlowToolbar, mRelativeLayoutSearchToolbar;

    @Override
    protected void initUi() {
        activitySetupProfileBinding = (ActivitySetupProfileBinding) viewDataBinding;
        nsvpContainer = activitySetupProfileBinding.nsvpContainer;
        mImageViewBackButton = activitySetupProfileBinding.toolbarInclude.imageViewBackButtonNsvp;
        mButtonSkip = activitySetupProfileBinding.toolbarInclude.buttonSkipToolbar;
        mButtonContinue = activitySetupProfileBinding.tvButtonContinueNsvp;
        mRelativeLayoutSignupFlowToolbar = activitySetupProfileBinding.toolbarInclude.rlNormalToolbar;
        mRelativeLayoutSearchToolbar = activitySetupProfileBinding.toolbarInclude.llSearchBar;
        mButtonDone = activitySetupProfileBinding.toolbarInclude.buttonDone;
        mButtonDone.setOnClickListener(this);
        mButtonContinue.setOnClickListener(this);
        mButtonSkip.setOnClickListener(this);
        mImageViewBackButton.setOnClickListener(this);
        SignupPagerAdapter signupPagerAdapter = new SignupPagerAdapter(getSupportFragmentManager());
        nsvpContainer.setOffscreenPageLimit(9);
        nsvpContainer.setCurrentItem(0);
        nsvpContainer.setAdapter(signupPagerAdapter);

    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_setup_profile;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_button_continue_nsvp:
                onContinueClick();
                break;
            case R.id.button_skip_toolbar:
                onContinueClick();
                break;
            case R.id.imageView_back_button_nsvp:
                onBackClicked();
                break;
            case R.id.button_done:
                startActivity(new Intent(SetupProfileActivity.this, EventFeedActivity.class));
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        onBackClicked();
    }

    private void onBackClicked() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.nsvp_container + ":" + activitySetupProfileBinding.nsvpContainer.getCurrentItem());
        if (fragment instanceof SignupMobileNumberFragment) {
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                finish();
                return;
            } else {
                mBackPressed = System.currentTimeMillis();
                Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show();
            }
        } else if (fragment instanceof SignupMobileNoConfirmationFragment) {
            nsvpContainer.setCurrentItem(0);
            mImageViewBackButton.setVisibility(View.VISIBLE);
            mButtonSkip.setVisibility(View.GONE);
        } else if (fragment instanceof SignupNameFragment) {
            nsvpContainer.setCurrentItem(1);
            mImageViewBackButton.setVisibility(View.VISIBLE);
            mButtonSkip.setVisibility(View.GONE);
        } else if (fragment instanceof SignupEmailFragment) {
            nsvpContainer.setCurrentItem(2);
            mImageViewBackButton.setVisibility(View.VISIBLE);
            mButtonSkip.setVisibility(View.GONE);
        } else if (fragment instanceof SignupBirthdayFragment) {
            nsvpContainer.setCurrentItem(3);
            mImageViewBackButton.setVisibility(View.VISIBLE);
            mButtonSkip.setVisibility(View.GONE);
        } else if (fragment instanceof SignupCreatePasswordFragment) {
            nsvpContainer.setCurrentItem(4);
            mButtonContinue.setText(R.string.btn_text_continue);
            mImageViewBackButton.setVisibility(View.VISIBLE);
            mButtonSkip.setVisibility(View.GONE);
        } else if (fragment instanceof SignupAddProfilePicFragment) {
            nsvpContainer.setCurrentItem(5);
            mButtonContinue.setText(R.string.create_account);
            mImageViewBackButton.setVisibility(View.VISIBLE);
            mButtonSkip.setVisibility(View.GONE);
        } else if (fragment instanceof SignupFindFacebookFriendsFragment) {
            nsvpContainer.setCurrentItem(6);
            mButtonContinue.setText(R.string.btn_text_add_photo);
            mImageViewBackButton.setVisibility(View.GONE);
            mButtonSkip.setVisibility(View.VISIBLE);
        } else if (fragment instanceof SignupAddFacebookFriendsFragment) {
            nsvpContainer.setCurrentItem(7);
            mImageViewBackButton.setVisibility(View.GONE);
            mButtonSkip.setVisibility(View.VISIBLE);
            mButtonContinue.setText(R.string.btn_text_connect_to_facebook);
            mRelativeLayoutSearchToolbar.setVisibility(View.GONE);
            mRelativeLayoutSignupFlowToolbar.setVisibility(View.VISIBLE);
            mButtonContinue.setVisibility(View.VISIBLE);
        }
    }

    private void onContinueClick() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.nsvp_container + ":" + activitySetupProfileBinding.nsvpContainer.getCurrentItem());
        if (fragment instanceof SignupMobileNumberFragment) {
            nsvpContainer.setCurrentItem(1);
            mImageViewBackButton.setVisibility(View.VISIBLE);
            mButtonSkip.setVisibility(View.GONE);
        } else if (fragment instanceof SignupMobileNoConfirmationFragment) {
            nsvpContainer.setCurrentItem(2);
            mImageViewBackButton.setVisibility(View.VISIBLE);
            mButtonSkip.setVisibility(View.GONE);
        } else if (fragment instanceof SignupNameFragment) {
            nsvpContainer.setCurrentItem(3);
            mImageViewBackButton.setVisibility(View.VISIBLE);
            mButtonSkip.setVisibility(View.GONE);
        } else if (fragment instanceof SignupEmailFragment) {
            nsvpContainer.setCurrentItem(4);
            mImageViewBackButton.setVisibility(View.VISIBLE);
            mButtonSkip.setVisibility(View.GONE);
        } else if (fragment instanceof SignupBirthdayFragment) {
            nsvpContainer.setCurrentItem(5);
            mButtonContinue.setText(R.string.create_account);
            mImageViewBackButton.setVisibility(View.VISIBLE);
            mButtonSkip.setVisibility(View.GONE);
        } else if (fragment instanceof SignupCreatePasswordFragment) {
            nsvpContainer.setCurrentItem(6);
            mButtonContinue.setText(R.string.btn_text_add_photo);
            mImageViewBackButton.setVisibility(View.GONE);
            mButtonSkip.setVisibility(View.VISIBLE);
        } else if (fragment instanceof SignupAddProfilePicFragment) {
            nsvpContainer.setCurrentItem(7);
            mButtonContinue.setText(R.string.btn_text_connect_to_facebook);
            mImageViewBackButton.setVisibility(View.GONE);
            mButtonSkip.setVisibility(View.VISIBLE);
        } else if (fragment instanceof SignupFindFacebookFriendsFragment) {
            nsvpContainer.setCurrentItem(8);
            mImageViewBackButton.setVisibility(View.GONE);
            mButtonSkip.setVisibility(View.GONE);
            mRelativeLayoutSearchToolbar.setVisibility(View.VISIBLE);
            mRelativeLayoutSignupFlowToolbar.setVisibility(View.GONE);
            mButtonContinue.setVisibility(View.GONE);
        }
    }
}
