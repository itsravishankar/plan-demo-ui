package com.example.ravi.plandemo.activity;

import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.ActivityLoginResetPasswordBinding;


public class PasswordResetActivity extends BaseActivity {

    private Button mButtonPasswordResetUsingPhone, mButtonPasswordResetUsingEmail;
    private EditText mEditTextPhoneNumber, mEditTextEmail;
    private TextView mTextViewCountryCodePicker, mTextViewEditTextLabel;
    private LinearLayout mLinearLayoutEmailEditTextLayout;
    private View mViewDivider;
    private ImageView mImageViewEmailValidationMsg, mImageViewBackButton;

    @Override
    protected void initUi() {
        ActivityLoginResetPasswordBinding activityLoginResetPasswordBinding = (ActivityLoginResetPasswordBinding) viewDataBinding;
        mButtonPasswordResetUsingEmail = activityLoginResetPasswordBinding.buttonEmailResetOption;
        mButtonPasswordResetUsingPhone = activityLoginResetPasswordBinding.buttonPhoneResetOption;
        mEditTextPhoneNumber = activityLoginResetPasswordBinding.editTextPhoneNumber;
        mTextViewCountryCodePicker = activityLoginResetPasswordBinding.tvCountryCodePicker;
        mLinearLayoutEmailEditTextLayout = activityLoginResetPasswordBinding.llEmailEditTextContainer;
        mViewDivider = activityLoginResetPasswordBinding.viewBtwPickerEdittext;
        mEditTextEmail = activityLoginResetPasswordBinding.editTextEmail;
        mImageViewBackButton = activityLoginResetPasswordBinding.imageViewBackButtonRp;
        mImageViewEmailValidationMsg = activityLoginResetPasswordBinding.imageViewResetEmailValidationIcon;
        mTextViewEditTextLabel = activityLoginResetPasswordBinding.textViewEditTextLabel;
        mButtonPasswordResetUsingPhone.setOnClickListener(this);
        mButtonPasswordResetUsingEmail.setOnClickListener(this);
        mEditTextPhoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        mEditTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mImageViewEmailValidationMsg.getVisibility() == View.GONE)
                    mImageViewEmailValidationMsg.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().contains("@") && charSequence.toString().contains(".com")) {
                    setImageViewAnimatedChange(PasswordResetActivity.this, mImageViewEmailValidationMsg, R.drawable.ic_done_white_16dp_2x);
                } else
                    setImageViewAnimatedChange(PasswordResetActivity.this, mImageViewEmailValidationMsg, R.drawable.ic_clear_white_18dp);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_login_reset_password;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.button_email_reset_option:
                onEmailButtonClick();
                break;
            case R.id.button_phone_reset_option:
                onPhoneButtonClick();
                break;
            case R.id.imageView_back_button_rp:
                onBackPressed();
                break;
        }
    }

    private void onPhoneButtonClick() {
        mEditTextPhoneNumber.setVisibility(View.VISIBLE);
        mTextViewCountryCodePicker.setVisibility(View.VISIBLE);
        mLinearLayoutEmailEditTextLayout.setVisibility(View.GONE);
        mButtonPasswordResetUsingPhone.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        mButtonPasswordResetUsingEmail.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        mViewDivider.setVisibility(View.VISIBLE);
        mTextViewEditTextLabel.setText(getString(R.string.tv_mobile_number));
    }

    private void onEmailButtonClick() {
        mEditTextPhoneNumber.setVisibility(View.GONE);
        mTextViewCountryCodePicker.setVisibility(View.GONE);
        mLinearLayoutEmailEditTextLayout.setVisibility(View.VISIBLE);
        mButtonPasswordResetUsingPhone.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        mButtonPasswordResetUsingEmail.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        mViewDivider.setVisibility(View.GONE);
        mTextViewEditTextLabel.setText(getString(R.string.tv_email));
    }
}
