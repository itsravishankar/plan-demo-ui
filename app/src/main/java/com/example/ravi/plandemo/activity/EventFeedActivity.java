package com.example.ravi.plandemo.activity;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.ActivityEventFeedBinding;
import com.example.ravi.plandemo.fragment.EventEmptyStateFragment;

public class EventFeedActivity extends BaseActivity {

    private ActivityEventFeedBinding activityEventFeedBinding;
    private LinearLayout mLinearLayoutContainer;
    private ImageView mImageViewLiveButton, mImageViewAddButton, mImageViewSearchButton, mImageViewCalendarButton;

    @Override
    protected void initUi() {
        activityEventFeedBinding = (ActivityEventFeedBinding) viewDataBinding;
        viewBinding();
        setListeners();
        //swapFragment();
        showLiveButton();
    }

    private void viewBinding() {
        mLinearLayoutContainer = activityEventFeedBinding.llMainContainerInclude.llMainContainer;
        mImageViewAddButton = activityEventFeedBinding.toolbarMainInclude.imageViewToolbarAddButton;
        mImageViewCalendarButton = activityEventFeedBinding.toolbarMainInclude.imageViewToolbarCalender;
        mImageViewSearchButton = activityEventFeedBinding.toolbarMainInclude.imageViewToolbarSearchButton;
        mImageViewLiveButton = activityEventFeedBinding.bottomBarInclude.imageViewLiveButton;
    }

    private void setListeners() {
        mImageViewSearchButton.setOnClickListener(this);
        mImageViewCalendarButton.setOnClickListener(this);
        mImageViewAddButton.setOnClickListener(this);
    }

    private void showLiveButton() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mImageViewLiveButton.setVisibility(View.VISIBLE);
                setImageViewAnimatedChange(EventFeedActivity.this, mImageViewLiveButton, R.drawable.ic_profile1);
            }
        }, 2000);
    }


    private void swapFragment() {
        EventEmptyStateFragment eventEmptyStateFragment = new EventEmptyStateFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(mLinearLayoutContainer.getId(), eventEmptyStateFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imageView_toolbar_add_button:
                startActivity(new Intent(EventFeedActivity.this, CreateEventActivity.class));
                break;
            case R.id.imageView_toolbar_search_button:

                break;
            case R.id.imageView_toolbar_calender:

                break;
        }
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_event_feed;
    }
}
