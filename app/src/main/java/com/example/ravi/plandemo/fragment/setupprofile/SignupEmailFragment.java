package com.example.ravi.plandemo.fragment.setupprofile;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.FragmentSignupEmailBinding;
import com.example.ravi.plandemo.fragment.BaseFragment;


public class SignupEmailFragment extends BaseFragment {

    private FragmentSignupEmailBinding fragmentSignupEmailBinding;
    private EditText mEditTextEmail;
    private ImageView mImageViewEmailValidationMsg;

    @Override
    protected void initUi() {
        fragmentSignupEmailBinding = (FragmentSignupEmailBinding) viewDataBinding;
        mEditTextEmail = fragmentSignupEmailBinding.editTextEmail;
        mImageViewEmailValidationMsg = fragmentSignupEmailBinding.imageViewEmailValidationIcon;

        mEditTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mImageViewEmailValidationMsg.getVisibility() == View.GONE)
                    mImageViewEmailValidationMsg.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().contains("@") && charSequence.toString().contains(".com")) {
                    setImageViewAnimatedChange(context, mImageViewEmailValidationMsg, R.drawable.ic_done_white_16dp_2x);
                } else
                    setImageViewAnimatedChange(context, mImageViewEmailValidationMsg, R.drawable.ic_clear_white_18dp);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_signup_email;
    }

    @Override
    public void onClick(View view) {

    }
}
