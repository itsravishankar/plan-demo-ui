package com.example.ravi.plandemo.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.ravi.plandemo.fragment.onboarding.OnBoardingFragmentOne;
import com.example.ravi.plandemo.fragment.onboarding.OnBoardingFragmentThree;
import com.example.ravi.plandemo.fragment.onboarding.OnBoardingFragmentTwo;


/**
 * The type My splash adapter.
 */
public class OnBoardingAdapter extends FragmentPagerAdapter {

    /**
     * Instantiates a new My splash adapter.
     *
     * @param fm the fm
     */

    public OnBoardingAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new OnBoardingFragmentOne();
            case 1:
                return new OnBoardingFragmentTwo();
            case 2:
                return new OnBoardingFragmentThree();
            default:
                return new OnBoardingFragmentOne();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
