package com.example.ravi.plandemo.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.databinding.ActivitySignupLoginBinding;



public class SignupLoginActivty extends BaseActivity {

    @Override
    protected void initUi() {
        ActivitySignupLoginBinding activitySignupLoginBinding = (ActivitySignupLoginBinding) viewDataBinding;
        RelativeLayout relativeLayoutCreateAccountButton = activitySignupLoginBinding.rlButtonCreateAccount;
        relativeLayoutCreateAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignupLoginActivty.this, SetupProfileActivity.class));
            }
        });
        Button mButtonLogin = activitySignupLoginBinding.buttonLogin;
        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignupLoginActivty.this, LoginActivity.class));
            }
        });
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_signup_login;
    }

    @Override
    public void onClick(View v) {

    }
}
