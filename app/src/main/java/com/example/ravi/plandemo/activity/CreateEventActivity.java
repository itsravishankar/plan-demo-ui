package com.example.ravi.plandemo.activity;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ravi.plandemo.R;
import com.example.ravi.plandemo.adapter.CreateEventPagerAdapter;
import com.example.ravi.plandemo.customui.NonSwipeableViewPager;
import com.example.ravi.plandemo.databinding.ActivityCreateEventBinding;
import com.example.ravi.plandemo.fragment.createEvent.CreateEventAddNameDetailsFragment;
import com.example.ravi.plandemo.fragment.createEvent.CreateEventDateTimePickerFragment;


public class CreateEventActivity extends BaseActivity {

    private static final long TIME_INTERVAL = 2000;
    private long mBackPressed;
    private ActivityCreateEventBinding activityCreateEventBinding;
    private NonSwipeableViewPager viewPagerCreateEvent;
    private TextView mTextViewContinueButton;
    private ImageView mImageViewBackButton;
    private ProgressBar mProgressBarCreateEvent;

    @Override
    protected void initUi() {
        activityCreateEventBinding = (ActivityCreateEventBinding) viewDataBinding;
        viewPagerCreateEvent = activityCreateEventBinding.nonSwipeableViewPagerCreateEvent;
        mTextViewContinueButton = activityCreateEventBinding.buttonContinueCreateEvent;
        mImageViewBackButton = activityCreateEventBinding.imageViewBackButtonCreateEvent;
        mProgressBarCreateEvent = activityCreateEventBinding.progressBarCreateEvent;
        mImageViewBackButton.setOnClickListener(this);
        mTextViewContinueButton.setOnClickListener(this);
        CreateEventPagerAdapter createEventPagerAdapter = new CreateEventPagerAdapter(getSupportFragmentManager());
        viewPagerCreateEvent.setOffscreenPageLimit(3);
        viewPagerCreateEvent.setCurrentItem(0);
        mProgressBarCreateEvent.setMax(100);
        mProgressBarCreateEvent.setBackgroundColor(Color.parseColor("#51c0b1"));
        viewPagerCreateEvent.setAdapter(createEventPagerAdapter);
        mProgressBarCreateEvent.setProgress(30);

    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_create_event;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imageView_back_button_create_event:
                onBackClicked();
                break;
            case R.id.button_continue_create_event:
                onContinueClicked();
                break;
        }
    }

    private void onBackClicked() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.nonSwipeableViewPager_create_event + ":" + viewPagerCreateEvent.getCurrentItem());
        if (fragment instanceof CreateEventAddNameDetailsFragment) {
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                finish();
                return;
            } else {
                mBackPressed = System.currentTimeMillis();
                Toast.makeText(this, "Press back again stop creating event", Toast.LENGTH_SHORT).show();
            }
        } else if (fragment instanceof CreateEventDateTimePickerFragment) {
            viewPagerCreateEvent.setCurrentItem(0);
            mProgressBarCreateEvent.setProgress(30);
        }
    }

    private void onContinueClicked() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.nonSwipeableViewPager_create_event + ":" + viewPagerCreateEvent.getCurrentItem());
        if (fragment instanceof CreateEventAddNameDetailsFragment) {
            viewPagerCreateEvent.setCurrentItem(1);
            mProgressBarCreateEvent.setProgress(60);
        }
    }
}
